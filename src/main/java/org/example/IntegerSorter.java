package org.example;

import java.util.Arrays;

/**
 * Sorting application that sorts command-line arguments.
 */
public class IntegerSorter {
    /**
     * Main method to run the app
     */
    public static void main(String[] args) {
        if (args.length > 10) {
            System.out.println("Error: More than 10 arguments provided.");
            return;
        }

        try {
            int[] numbers = Arrays.stream(args).mapToInt(Integer::parseInt).toArray();
            Arrays.sort(numbers);
            System.out.println(Arrays.toString(numbers));
        } catch (NumberFormatException e) {
            System.out.println("Error: All numbers should be integers.");
        }
    }
}
