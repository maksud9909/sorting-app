

import org.example.IntegerSorter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class IntegerSorterTest {

    private final String[] input;
    private final String expectedOutput;

    public IntegerSorterTest(String[] input, String expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, "[]"},
                {new String[]{"3"}, "[3]"},
                {new String[]{"9", "1", "8", "2", "7", "3", "6", "4", "5", "0"}, "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"},
                {new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1", "0"}, "Error: More than 10 arguments provided."},
                {new String[]{"a", "b"}, "Error: All numbers should be integers."}
        });
    }

    @Test
    public void testApp() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        IntegerSorter.main(input);

        assertEquals(expectedOutput, outContent.toString().trim());
    }
}
